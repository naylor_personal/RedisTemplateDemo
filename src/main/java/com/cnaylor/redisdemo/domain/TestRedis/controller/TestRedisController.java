package com.cnaylor.redisdemo.domain.TestRedis.controller;


import com.alibaba.fastjson.JSON;
import com.cnaylor.redisdemo.domain.TestRedis.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/redis/")
public class TestRedisController {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;


    @GetMapping("test")
    public String test() {
        return " Hello World !";
    }

    /**
     * String类型set
     *
     * @param key
     * @param value
     * @return
     */
    @GetMapping("string/set/{key}/{value}")
    public boolean stringSet(@PathVariable("key") String key, @PathVariable("value") String value) {
        redisTemplate.opsForValue().set(key, value);
        return true;
    }

    /**
     * String类型get
     *
     * @param key
     * @return
     */
    @GetMapping("string/get/{key}")
    public String stringGet(@PathVariable("key") String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * Hash类型ser
     *
     * @param key
     * @return
     */
    @GetMapping("hash/set/{key}")
    public boolean hashSet(@PathVariable("key") Integer key) {
        UserDto userDto = new UserDto().setId(key).setName("redis").setStature(0.55D);
        Map<String, String> map = new Hashtable<>();
        map.put(key.toString(), JSON.toJSONString(userDto));
        redisTemplate.opsForHash().putAll(key.toString(), map);
        return true;
    }

    /**
     * Hash类型ser
     *
     * @param key
     * @param field
     * @param value
     * @return
     */
    @GetMapping("hash/set/{key}/{field}/{value}")
    public boolean hashSet(@PathVariable("key") String key, @PathVariable("field") String field, @PathVariable("value") String value) {
        redisTemplate.opsForHash().put(key, field, value);
        return true;
    }

    /**
     * Hash类型get
     *
     * @param key
     * @return
     */
    @GetMapping("hash/get/{key}")
    public UserDto hashGet(@PathVariable("key") String key) {
        String result = JSON.toJSONString(redisTemplate.opsForHash().get(key, key));
        return JSON.parseObject(result, UserDto.class);
    }

    /**
     * Hash类型get
     *
     * @param key
     * @param field
     * @return
     */
    @GetMapping("hash/get/{key}/{field}")
    public String hashGet(@PathVariable("key") String key, @PathVariable("field") String field) {
        return JSON.toJSONString(redisTemplate.opsForHash().get(key, field));
    }

    /**
     * List类型set - 一个一个的增加
     *
     * @param key
     * @param value
     * @return
     */
    @GetMapping("list/set/{key}/{value}")
    public boolean listSet(@PathVariable("key") String key, @PathVariable("value") String value) {
        String[] array = value.split(",");
        for (String item : array) {
            redisTemplate.opsForList().leftPush(key, item);
        }
        return true;
    }

    /**
     * List类型get
     *
     * @param key
     * @return
     */
    @GetMapping("list/get/{key}")
    public List<String> listGet(@PathVariable("key") String key) {
        return redisTemplate.opsForList().range(key, 0, -1);
    }

    /**
     * Set类型set
     *
     * @param key
     * @param value
     * @return
     */
    @GetMapping("set/set/{key}/{value}")
    public boolean setSet(@PathVariable("key") String key, @PathVariable("value") String value) {
        String[] array = value.split(",");
        for (String item : array) {
            redisTemplate.opsForSet().add(key, item);
        }
        return true;
    }

    /**
     * Set类型get
     *
     * @param key
     * @return
     */
    @GetMapping("set/get/{key}")
    public Set<String> setGet(@PathVariable("key") String key) {
        return redisTemplate.opsForSet().members(key);
    }


}
