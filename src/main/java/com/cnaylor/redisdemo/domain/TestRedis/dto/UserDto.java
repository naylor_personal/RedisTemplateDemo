package com.cnaylor.redisdemo.domain.TestRedis.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class UserDto implements Serializable {
    private Integer id;

    private String name;

    private Double stature;
}
